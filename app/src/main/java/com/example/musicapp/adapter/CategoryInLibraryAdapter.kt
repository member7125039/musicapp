package com.example.musicapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseAdapterRecyclerView
import com.example.musicapp.base.utils.extension.GlideUtil
import com.example.musicapp.databinding.LayoutItemCategoryInLibraryBinding
import com.example.musicapp.entities.Category

class CategoryInLibraryAdapter :
    BaseAdapterRecyclerView<Category, LayoutItemCategoryInLibraryBinding>() {

    private var onClickMoreCategory: ((item: Category, position: Int) -> Unit)? = null

    fun setOnMoreCategory(listener: ((item: Category, position: Int) -> Unit)? = null) {
        onClickMoreCategory = listener
    }

    private val roundBitmap by lazy {
        GlideUtil.createRoundedTransform(80)
    }

    override fun inflateBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): LayoutItemCategoryInLibraryBinding {
        return LayoutItemCategoryInLibraryBinding.inflate(inflater, parent, false)
    }

    override fun bindData(
        binding: LayoutItemCategoryInLibraryBinding,
        item: Category,
        position: Int
    ) {
        Glide.with(binding.root.context)
            .load(item.image)
            .apply(roundBitmap)
            .placeholder(R.drawable.logo_blue)
            .error(R.drawable.logo_blue)
            .into(binding.imgCategory)
        binding.nameCategory.text = item.name
        binding.moreCategory.setOnClickListener {
            onClickMoreCategory?.invoke(item, position)
        }
    }
}