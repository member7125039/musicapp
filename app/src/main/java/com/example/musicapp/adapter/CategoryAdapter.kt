package com.example.musicapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.musicapp.base.base_view.BaseAdapterRecyclerView
import com.example.musicapp.databinding.LayoutItemCategoryBinding
import com.example.musicapp.entities.Category

class CategoryAdapter : BaseAdapterRecyclerView<Category, LayoutItemCategoryBinding>() {
    override fun inflateBinding(
        inflater: LayoutInflater, parent: ViewGroup
    ): LayoutItemCategoryBinding {
        return LayoutItemCategoryBinding.inflate(inflater, parent, false)
    }

    override fun bindData(binding: LayoutItemCategoryBinding, item: Category, position: Int) {
        binding.nameCategory.text = item.name
        Glide.with(binding.root.context).load(item.image).into(binding.imgCategory)
    }
}