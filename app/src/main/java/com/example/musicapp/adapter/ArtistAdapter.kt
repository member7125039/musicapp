package com.example.musicapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.musicapp.base.base_view.BaseAdapterRecyclerView
import com.example.musicapp.databinding.LayoutItemArtistBinding
import com.example.musicapp.entities.Artist

class ArtistAdapter : BaseAdapterRecyclerView<Artist, LayoutItemArtistBinding>() {
    override fun inflateBinding(
        inflater: LayoutInflater, parent: ViewGroup
    ): LayoutItemArtistBinding {
        return LayoutItemArtistBinding.inflate(inflater, parent, false)
    }

    override fun bindData(binding: LayoutItemArtistBinding, item: Artist, position: Int) {
        binding.nameArtist.text = item.fullname
        Glide.with(binding.root.context).load(item.avatar).into(binding.imgArtist)
    }
}