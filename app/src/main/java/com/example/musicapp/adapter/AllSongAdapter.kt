package com.example.musicapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.musicapp.base.base_view.BaseAdapterRecyclerView
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.databinding.LayoutItemAllSongBinding
import com.example.musicapp.entities.Song

class AllSongAdapter : BaseAdapterRecyclerView<Song, LayoutItemAllSongBinding>() {
    override fun inflateBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): LayoutItemAllSongBinding {
        return LayoutItemAllSongBinding.inflate(inflater, parent, false)
    }

    override fun bindData(binding: LayoutItemAllSongBinding, item: Song, position: Int) {
        binding.nameSong.text = item.title
        //TODO: tìm hiểu thêm các transform, option với Glide (asBitmap, resize, RoundedCorner, placeHolder, error, cache)
        FunctionUtils.loadImage(binding.root.context,binding.imgBackground,item.image,22)
    }
}