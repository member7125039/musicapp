package com.example.musicapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseAdapterRecyclerView
import com.example.musicapp.base.entity.ListCommentOfSongResponse
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.extension.GlideUtil
import com.example.musicapp.databinding.LayoutItemCommentBinding

class CommentsOfSongAdapter :
    BaseAdapterRecyclerView<ListCommentOfSongResponse, LayoutItemCommentBinding>() {
    companion object {
        @SuppressLint("NonConstantResourceId")
        @DrawableRes
        const val PLACE_HOLDER_RES = R.drawable.logo_blue
        @SuppressLint("NonConstantResourceId")
        @DrawableRes
        const val ERROR_RES = R.drawable.logo_blue
        const val userIdDefault = "64c271e396695b98facf9caf"
        val roundedCorner25 = GlideUtil.createRoundedTransform(25)
    }

    override fun inflateBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): LayoutItemCommentBinding {
        return LayoutItemCommentBinding.inflate(inflater, parent, false)
    }

    override fun bindData(
        binding: LayoutItemCommentBinding,
        item: ListCommentOfSongResponse,
        position: Int
    ) {
        binding.comment.text = item.content
        Constant.users.find { user -> item.user == user.id }?.let { user ->
            val url = if (user.id == userIdDefault) {
                user.avatar
            } else {
                user.getUrlAvatar()
            }
            Glide.with(binding.root.context).load(url)
                .apply(roundedCorner25)
                .placeholder(PLACE_HOLDER_RES)
                .error(ERROR_RES)
                .into(binding.imgArtist)
            binding.nameArtist.text = user.fullname
        }
    }
}