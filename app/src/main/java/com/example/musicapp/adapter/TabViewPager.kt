package com.example.musicapp.adapter

enum class TabViewPager {
    HOME, EXPLORE, LIBRARY, PROFILE;

    companion object {
        fun getTabByPosition(position: Int): TabViewPager {
            return values().getOrNull(position) ?: HOME
        }

        fun getTabCount() = values().size

    }
}