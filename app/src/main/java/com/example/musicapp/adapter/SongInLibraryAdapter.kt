package com.example.musicapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseAdapterRecyclerView
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.extension.GlideUtil
import com.example.musicapp.databinding.LayoutItemArtistInSongBinding
import com.example.musicapp.entities.Song

class SongInLibraryAdapter :
    BaseAdapterRecyclerView<Song, LayoutItemArtistInSongBinding>() {

    private var onClickPlay: ((item: Song, position: Int) -> Unit)? = null

    fun setOnClickPlay(listener: ((item: Song, position: Int) -> Unit)? = null) {
        onClickPlay = listener
    }

    private val roundBitmap by lazy {
        GlideUtil.createRoundedTransform(25)
    }

    override fun inflateBinding(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): LayoutItemArtistInSongBinding {
        return LayoutItemArtistInSongBinding.inflate(inflater, parent, false)
    }

    override fun bindData(
        binding: LayoutItemArtistInSongBinding,
        item: Song,
        position: Int
    ) {
        Glide.with(binding.root.context)
            .load(item.image)
            .apply(roundBitmap)
            .placeholder(R.drawable.logo_blue)
            .error(R.drawable.logo_blue)
            .into(binding.imgSong)

        binding.nameSong.text = item.title

        Constant.listArtist.find { it.id == item.singer }?.let {
            binding.nameArtist.text = it.fullname
        } ?: kotlin.run {
            binding.nameArtist.text = item.title
        }
        binding.play.setOnClickListener {
            onClickPlay?.invoke(item, position)
        }
    }
}