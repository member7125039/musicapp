package com.example.musicapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.musicapp.screen.explore.ExploreFragment
import com.example.musicapp.screen.home.HomeFragment
import com.example.musicapp.screen.library.LibraryFragment
import com.example.musicapp.screen.profile.ProfileFragment

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount() = TabViewPager.getTabCount()
    override fun createFragment(position: Int): Fragment {
        return when (TabViewPager.getTabByPosition(position)) {
            TabViewPager.HOME -> HomeFragment()
            TabViewPager.EXPLORE -> ExploreFragment()
            TabViewPager.LIBRARY -> LibraryFragment()
            TabViewPager.PROFILE -> ProfileFragment()
        }
    }
}