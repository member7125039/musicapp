package com.example.musicapp.base.viewmodel

import com.example.musicapp.base.base_view.BaseViewModel
import com.example.musicapp.base.base_view.DataResult
import com.example.musicapp.base.entity.SearchResponse
import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient
import com.example.musicapp.base.utils.enqueueShort
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

open class ExploreViewModel : BaseViewModel() {
    private val apiService by lazy {
        RetrofitClient.getInstance().create(ApiService::class.java)
    }

    fun getDataSearching(
        accessToken: String,
        titleSearch: String,
        onDone: (SearchResponse) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(
            request = { getDataSearch(accessToken, titleSearch) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {
                onError.invoke("${it.message}")
            },
            showLoading = true
        )
    }

    private suspend fun getDataSearch(
        accessToken: String,
        titleSearch: String
    ): DataResult<SearchResponse> {
        return suspendCoroutine { continuation ->
            val call = apiService.getDataSearching("Bearer $accessToken", titleSearch)
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body() != null) {
                    val data = response.body()!!
                    continuation.resume(DataResult.Success(data))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message)))
            })
        }
    }
}