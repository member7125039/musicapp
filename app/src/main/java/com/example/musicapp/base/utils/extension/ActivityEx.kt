package com.example.musicapp.base.utils.extension

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.musicapp.base.utils.Constant

fun AppCompatActivity.replaceFragment(
    frameId: Int, fragment: Fragment, addToBackStack: Boolean = true, bundle: Bundle? = null
) {
    bundle?.let {
        fragment.arguments = bundle
    }
    if (addToBackStack) {
        replaceFragmentAddBackStack(frameId, fragment)
    } else {
        replaceFragmentNoAddBackStack(frameId, fragment)
    }
}

fun AppCompatActivity.replaceFragmentAddBackStack(frameId: Int, fragment: Fragment) {
    Log.d(Constant.TAG, "replaceFragment  add backstack:${fragment.javaClass.name}")
    supportFragmentManager.beginTransaction().replace(frameId, fragment)
        .addToBackStack(fragment.javaClass.name).commit()
}

fun AppCompatActivity.replaceFragmentNoAddBackStack(frameId: Int, fragment: Fragment) {
    Log.d(Constant.TAG, "replaceFragment add no backstack:{fragment.javaClass.name}")
    supportFragmentManager.beginTransaction().replace(frameId, fragment).commit()
}

fun ComponentActivity.handleBackPressed(action: () -> Unit) {
    onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            action()
        }
    })
}