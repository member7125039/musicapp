package com.example.musicapp.base.entity

data class DataCommentResponse(
    val user: String,
    val music: String,
    val content: String,
    val id: String,
)
