package com.example.musicapp.base.entity

data class AllUserResponse(
    val status:Int,
    val message:String,
    val data:DataAllUser
)
