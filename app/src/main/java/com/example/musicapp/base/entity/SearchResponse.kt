package com.example.musicapp.base.entity

data class SearchResponse(
    val status: Int,
    val message: String,
    val data: MusicData
)

data class MusicData(
    val musics: List<Music>,
    val singers: List<Singer>,
    val categories: List<Category1>
)

data class Music(
    val singer: Singer,
    val title: String,
    val description: String,
    val country: String,
    val image: String,
    val audio: String,
    val year: Int,
    val length: Int,
    val playCount: Int,
    val categoryId: String,
    val createdAt: String,
    val id: String
)

data class Singer(
    val fullname: String,
    val birthday: String?,
    val gender: String,
    val avatar: String,
    val description: String?,
    val createdAt: String,
    val id: String
)

data class Category1(
    val name: String,
    val image: String,
    val createdAt: String,
    val id: String
)

