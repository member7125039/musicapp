package com.example.musicapp.base.entity

data class ListCommentOfSongResponse(
    var user:String,
    var content:String,
    var id:String
)
