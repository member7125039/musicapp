package com.example.musicapp.base.utils.extension

import android.os.Build

fun isSdk33() = isSdkTIRAMISU()
fun isSdkTIRAMISU() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU