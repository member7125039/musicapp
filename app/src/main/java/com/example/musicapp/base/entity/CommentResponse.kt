package com.example.musicapp.base.entity


data class CommentResponse(
    val status: Int,
    val message: String,
    val data: DataCommentResponse
)
