package com.example.musicapp.base.entity

import com.example.musicapp.entities.Category

data class ListCategoryResult(
    val results: List<Category>
)
