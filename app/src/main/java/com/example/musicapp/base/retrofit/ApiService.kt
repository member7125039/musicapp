package com.example.musicapp.base.retrofit

import com.example.musicapp.base.entity.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @POST("/api/auth/login")
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @POST("/api/auth/register")
    fun register(@Body registerRequest: RegisterRequest): Call<RegisterResponse>

    @POST("/api/comments")
    fun pushComment(
        @Header("Authorization")accessToken: String,
        @Body commentRequest: CommentRequest
    ): Call<CommentResponse>

    @GET("/api/musics?limit=200")
    fun getSong(@Header("Authorization") accessToken: String): Call<ListSongResponse>

    @GET("/api/singers?limit=200")
    fun getArtist(@Header("Authorization") accessToken: String): Call<ListArtistResponse>

    @GET("/api/categories")
    fun getCategory(@Header("Authorization") accessToken: String): Call<ListCategoryResponse>

    @GET("/api/auth/profile")
    fun getProfile(@Header("Authorization") accessToken: String): Call<UserResponse>

    @GET("/api/musics/singer/{singerId}")
    fun getSongOfArtist(
        @Header("Authorization") accessToken: String,
        @Path("singerId") id: String
    ): Call<ListSongOfArtistResponse>

    @GET("/api/musics/{ID}")
    fun getSongWithId(
        @Header("Authorization") accessToken: String,
        @Path("ID") id: String
    ): Call<SongWithIdResponse>

    @GET("/api/musics/category/{categoryId}")
    fun getSongWithCategory(
        @Header("Authorization") accessToken: String,
        @Path("categoryId") id: String
    ): Call<SongWithCategoryResponse>

    @GET("/api/comments/music/{musicId}")
    fun getCommentOfSong(
        @Header("Authorization") accessToken: String,
        @Path("musicId") id: String
    ): Call<CommentOfSongResponse>

    @GET("/api/users?limit=200")
    fun getAllUser(@Header("Authorization") accessToken: String): Call<AllUserResponse>

    @GET("/api/musics/search")
    fun getDataSearching(
        @Header("Authorization") accessToken: String,
        @Query("keyword") titleSearch:String
    ):Call<SearchResponse>
}