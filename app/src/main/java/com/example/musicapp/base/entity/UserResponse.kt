package com.example.musicapp.base.entity

import com.example.musicapp.entities.User

data class UserResponse(
    val status: String,
    val message: String,
    val data: User
)
