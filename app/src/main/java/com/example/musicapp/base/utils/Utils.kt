package com.example.musicapp.base.utils

import android.annotation.SuppressLint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

/**
 * retrofit enqueue
 */
fun <T> Call<T>.enqueueShort(success: ((Response<T>) -> Unit)? = null, failed: ((Throwable) -> Unit)? = null) {
    this.enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            success?.invoke(response)
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            failed?.invoke(t)
        }
    })
}

@SuppressLint("SimpleDateFormat")
fun String.formatBirthday(): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd")
    val outputFormat = SimpleDateFormat("yyyy - MM - dd")
    return try {
        //nếu sai có thể bị crash (xem cái throw của nó)
        val newBirthday = inputFormat.parse(this)
        newBirthday?.let { outputFormat.format(it) } ?: ""
    } catch (ex: Exception) {
        ""
    }
}

fun Int.timerConvert(): String {
    val hrs = this / 3600
    val mns = this % 3600 / 60
    val scs = this % 60

    return if (hrs > 0) {
        String.format("%02d:%02d:%02d", hrs, mns, scs)
    } else {
        String.format("%02d:%02d", mns, scs)
    }
}