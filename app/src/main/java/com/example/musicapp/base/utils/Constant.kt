package com.example.musicapp.base.utils

import android.content.res.Resources
import com.example.musicapp.base.entity.ListCommentOfSongResponse
import com.example.musicapp.entities.Artist
import com.example.musicapp.entities.Category
import com.example.musicapp.entities.Song
import com.example.musicapp.entities.SongOfArtist
import com.example.musicapp.entities.User

object Constant {
    var languageDefault = Resources.getSystem().configuration.locales[0].language
    const val ITEM = "item"
    const val ID = "id"
    const val DATA = "DATA"
    const val USER = "user"
    const val IDSONGCOMMENT="idSongComment"
    const val POSITION = "position"
    const val SINGERID = "singerId"
    const val CATEGORYID = "categoryId"
    const val NAMEARTIST = "nameArtist"
    const val NAMECATEGORY = "nameCategory"
    const val ISREMEMBER = "isRemember"
    const val USERNAME = "userName"
    const val PASSWORD = "password"
    const val ARTIST = "artist"
    const val READ_TIME_OUT: Long = 30
    const val CONNECT_TIME_OUT: Long = 30
    const val TAG = "ledung"
    const val SHARED_PREFERENCES: String = "my_sharedPreferences"
    const val ACCESS_TOKEN: String = "accessToken"
    const val KEY_REMEMBER = "isRemember"
    const val KEY_USER_NAME = "userName"
    const val KEY_PASSWORD = "password"
    var listSong: MutableList<Song> = mutableListOf()
    var listArtist: MutableList<Artist> = mutableListOf()
    var listCategory: MutableList<Category> = mutableListOf()
    var listSongOfCategory: MutableList<SongOfArtist> = mutableListOf()
    var comments: MutableList<ListCommentOfSongResponse> = mutableListOf()
    var users: MutableList<User> = mutableListOf()
    var profile: User= User()
    var newComment=false

}