package com.example.musicapp.base.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.utils.extension.GlideUtil
import com.example.musicapp.base.utils.extension.pushBundle
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.entities.SongOfArtist
import com.example.musicapp.screen.play.PlayingFragment

class FunctionUtils {
    companion object {
        fun playingSongChoose(
            mainActivity: MainActivity,
            item: SongOfArtist?
        ) {
            Constant.listSong.find { item?.id == it.id }?.let { song ->
                SharePrefUtils.saveKey(Constant.ID, song.id)
                val bundle = pushBundle(Constant.ITEM, song)
                mainActivity.replaceFragment(R.id.main, PlayingFragment(), true, bundle)
            }
        }

        fun loadImage(
            context: Context,
            imageView: ImageView,
            imageUrl: String?,
            radius: Int
        ) {
            Glide.with(context)
                .load(imageUrl)
                .apply(
                    GlideUtil.createRoundedTransform(radius)
                )
                .placeholder(R.drawable.logo_blue)
                .error(R.drawable.logo_blue)
                .into(imageView)
        }

        fun showAndLoadingPleaseWait(context: Context, dialog: AlertMessageDialog,show: Boolean) {
            if (show) {
                dialog.show(
                    context.getString(R.string.notification),
                    context.getString(R.string.please_wait),
                    "",
                    ""
                )
            } else {
                dialog.hide()
            }
        }
    }
}