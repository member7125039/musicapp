package com.example.musicapp.base.viewmodel


import android.content.Context
import android.widget.Toast
import com.example.musicapp.base.base_view.BaseViewModel
import com.example.musicapp.base.base_view.DataResult
import com.example.musicapp.base.entity.DataSongWithIdResponse
import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.enqueueShort
import com.example.musicapp.entities.Artist
import com.example.musicapp.entities.Category
import com.example.musicapp.entities.Song
import com.example.musicapp.entities.SongOfArtist
import com.example.musicapp.entities.User
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


open class HomeViewModel : BaseViewModel() {

    private val apiService by lazy {
        RetrofitClient.getInstance().create(ApiService::class.java)
    }

    fun getDataSongWithCategory(
        context: Context,
        accessToken: String,
        categoryId: String,
        onDone: (MutableList<SongOfArtist>) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(
            request = { getDataOfSongWithCategory(context, accessToken, categoryId) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {
                onError.invoke("${it.message}")
            },
            showLoading = true
        )
    }

    private suspend fun getDataOfSongWithCategory(
        context: Context,
        accessToken: String,
        categoryId: String
    ): DataResult<MutableList<SongOfArtist>> {
        SharePrefUtils.init(context)
        var list: MutableList<SongOfArtist>
        return suspendCoroutine { continuation ->
            val call = apiService.getSongWithCategory("Bearer $accessToken", categoryId)
            call.enqueueShort(success = { response ->
                SharePrefUtils.saveKey(Constant.NAMECATEGORY, response.body()?.data?.category)
                if (response.isSuccessful && response.body()?.data?.musics != null) {
                    list = response.body()?.data!!.musics.toMutableList()
                    continuation.resume(DataResult.Success(list))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message)))
            })
        }
    }

    fun getDataSongWithId(
        context: Context,
        accessToken: String,
        songId: String,
        onDone: (DataSongWithIdResponse) -> Unit
    ) {
        executeTask(
            request = { getDataOfSongWithId(context, accessToken, songId) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {

            },
            showLoading = true
        )
    }

    private suspend fun getDataOfSongWithId(
        context: Context, accessToken: String, songId: String
    ): DataResult<DataSongWithIdResponse> {
        return suspendCoroutine { continuation ->
            val call = apiService.getSongWithId("Bearer $accessToken", songId)
            call.enqueueShort(success = { response ->
                if (response.body() != null && response.isSuccessful) {
                    val songWithIdResponse = response.body()
                    if (songWithIdResponse != null) {
                        val data = songWithIdResponse.data
                        continuation.resume(DataResult.Success(data))
                    }
                }
            }, failed = {
                Toast(context)
            })
        }
    }

    fun getSongOfArtist(
        accessToken: String,
        singerId: String,
        onDone: (MutableList<SongOfArtist>) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(
            request = { getListSongOfArtist(accessToken, singerId) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {
                onError.invoke("${it.message}")
            },
            showLoading = true
        )
    }

    private suspend fun getListSongOfArtist(
        accessToken: String, singerId: String
    ): DataResult<MutableList<SongOfArtist>> {
        var list: MutableList<SongOfArtist>
        return suspendCoroutine { continuation ->
            val call = apiService.getSongOfArtist("Bearer $accessToken", singerId)
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data?.musics != null) {
                    list = response.body()?.data!!.musics.toMutableList()
                    continuation.resume(DataResult.Success(list))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message)))
            })
        }
    }

    fun getProfile(
        accessToken: String,
        onDone: (User) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(request = { getUser(accessToken) }, onSuccess = { result ->
            onDone.invoke(result)
        }, onError = {
            onError.invoke("${it.message}")
        }, showLoading = true
        )
    }

    private suspend fun getUser(accessToken: String): DataResult<User> {
        var user: User
        return suspendCoroutine { continuation ->
            val call = apiService.getProfile("Bearer $accessToken ")
            call.enqueueShort(success = { response ->
                if (response.body() != null && response.isSuccessful) {
                    user = response.body()?.data!!
                    continuation.resume(DataResult.Success(user))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message ?: "Network Error")))
            })
        }

    }

    fun getCategory(
        accessToken: String,
        onDone: (MutableList<Category>) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(
            request = { getListCategory(accessToken) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {
                onError.invoke("${it.message}")
            },
            showLoading = true
        )
    }

    private suspend fun getListCategory(accessToken: String): DataResult<MutableList<Category>> {
        var list: MutableList<Category>
        return suspendCoroutine { continuation ->
            val call = apiService.getCategory("Bearer $accessToken")
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data != null) {
                    list = response.body()?.data!!.results.toMutableList()
                    continuation.resume(DataResult.Success(list))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(java.lang.Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message)))
            })
        }
    }

    fun getArtistAtHome(
        accessToken: String,
        onDone: (MutableList<Artist>) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(request = { getListArtist(accessToken) },
            onSuccess = { result ->
                onDone.invoke(result)
            }, onError = {
                onError.invoke("${it.message}")
            }, showLoading = true
        )
    }

    private suspend fun getListArtist(accessToken: String): DataResult<MutableList<Artist>> {
        var list: MutableList<Artist>
        return suspendCoroutine { continuation ->
            val call = apiService.getArtist("Bearer $accessToken")
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data != null) {
                    list = response.body()?.data!!.results.toMutableList()
                    continuation.resume(DataResult.Success(list))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message)))
            })
        }
    }

    fun getSongAtHome(
        accessToken: String,
        onDone: (MutableList<Song>) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(request = { getListSong(accessToken) }, onSuccess = { result ->
            onDone.invoke(result)
        }, onError = {
            onError.invoke("${it.message}")
        }, showLoading = true
        )
    }

    private suspend fun getListSong(accessToken: String): DataResult<MutableList<Song>> {
        var list: MutableList<Song>
        return suspendCoroutine { continuation ->
            val call = apiService.getSong("Bearer $accessToken")
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data != null) {
                    list = response.body()?.data!!.results.toMutableList()
                    continuation.resume(DataResult.Success(list))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message)))
            })
        }
    }
}
