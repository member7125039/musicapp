package com.example.musicapp.base.viewmodel

import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient

open class LibraryViewModel {
    private val apiService by lazy {
        RetrofitClient.getInstance().create(ApiService::class.java)
    }

}