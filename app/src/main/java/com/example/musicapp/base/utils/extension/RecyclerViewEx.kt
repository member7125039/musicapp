package com.example.musicapp.base.utils.extension

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setLinearLayoutManager(
    context: Context, holderAdapter: RecyclerView.Adapter<*>, orientation: Int
) {
    val manager = LinearLayoutManager(context, orientation, false)
    this.apply {
        layoutManager = manager
        adapter = holderAdapter
    }
}

fun RecyclerView.setGridManager(
    mContext: Context,
    lin: Int,
    holderAdapter: RecyclerView.Adapter<*>,
    orientation: Int = RecyclerView.VERTICAL,
) {
    val manager = GridLayoutManager(mContext, lin, orientation, false)
    this.apply {
        layoutManager = manager
        adapter = holderAdapter
    }
}