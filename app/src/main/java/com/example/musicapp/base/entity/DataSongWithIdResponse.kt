package com.example.musicapp.base.entity


data class DataSongWithIdResponse(
    val singer: SingerOfSongWithIdResponse,
    val title: String,
    val description: String,
    val country: String,
    val image: String,
    val audio: String,
    val year: Int,
    val length: Int,
    val playCount: Int,
    val category: CategoryOfSongWithIdResponse,
    val id: String
)
