package com.example.musicapp.base.entity

data class CategoryOfSongWithIdResponse(
    val name: String,
    val image: String,
    val createdAt: String,
    val id: String
)
