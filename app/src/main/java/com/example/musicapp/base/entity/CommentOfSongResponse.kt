package com.example.musicapp.base.entity

data class CommentOfSongResponse(
    var status:Int,
    var message:String,
    var data:DataCommentOfSongResponse
)
