package com.example.musicapp.base.retrofit

import com.example.musicapp.base.utils.Constant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {
    private const val URL = "https://hitmusic.tech"
    private var INSTANCE: Retrofit? = null
    fun getInstance(): Retrofit =
        INSTANCE ?: synchronized(this) {
            INSTANCE ?: retrofitBuilder().also {
                INSTANCE = it
            }
        }

    private val interceptor by lazy {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }
    private val client: OkHttpClient by lazy {
        OkHttpClient.Builder().readTimeout(Constant.READ_TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(Constant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor).build()
    }

    private val gsonConverterFactory by lazy {
        GsonConverterFactory.create()
    }

    private fun retrofitBuilder(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(gsonConverterFactory)
            .client(client)
            .build()
    }
}