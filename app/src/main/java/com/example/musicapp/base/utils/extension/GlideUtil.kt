package com.example.musicapp.base.utils.extension

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

object GlideUtil {

    fun createRoundedTransform(angle: Int): RequestOptions {
        return RequestOptions.bitmapTransform(RoundedCorners(angle))
    }

    fun loadImage(
        context: Context,
        url: String?, transform: RequestOptions, placeHolder: Int,
        error: Int, imageView: ImageView
    ) {
        Glide.with(context)
            .load(url)
            .apply(transform)
            .placeholder(placeHolder)
            .error(error)
            .into(imageView)
    }
}