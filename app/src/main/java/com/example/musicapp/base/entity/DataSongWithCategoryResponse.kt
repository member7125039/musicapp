package com.example.musicapp.base.entity

import com.example.musicapp.entities.SongOfArtist

data class DataSongWithCategoryResponse(
    val category: String,
    val musics: MutableList<SongOfArtist>
)
