package com.example.musicapp.base.entity

data class ListCategoryResponse(
    val status: String,
    val message: String,
    val data: ListCategoryResult
)
