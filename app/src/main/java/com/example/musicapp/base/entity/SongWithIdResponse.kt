package com.example.musicapp.base.entity

data class SongWithIdResponse(
    val status: Int,
    val message: String,
    val data: DataSongWithIdResponse

)
