package com.example.musicapp.base.entity

data class DataCommentOfSongResponse(
    var music:String,
    var comments:MutableList<ListCommentOfSongResponse>
)
