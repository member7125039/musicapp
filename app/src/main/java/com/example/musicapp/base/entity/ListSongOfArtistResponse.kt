package com.example.musicapp.base.entity

data class ListSongOfArtistResponse(
    val status: Int,
    val message: String,
    val data: DataListSongOfArtistResponse
)
