package com.example.musicapp.base.entity

data class SingerOfSongWithIdResponse(
    val fullname: String,
    val avatar: String,
    val id: String
)
