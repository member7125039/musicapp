package com.example.musicapp.base.entity

data class CommentRequest(
    val music: String,
    val content: String
)
