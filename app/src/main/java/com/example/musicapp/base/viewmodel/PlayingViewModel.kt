package com.example.musicapp.base.viewmodel

import com.example.musicapp.base.base_view.BaseViewModel
import com.example.musicapp.base.base_view.DataResult
import com.example.musicapp.base.entity.CommentRequest
import com.example.musicapp.base.entity.ListCommentOfSongResponse
import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient
import com.example.musicapp.base.utils.enqueueShort
import com.example.musicapp.entities.User
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

open class PlayingViewModel : BaseViewModel() {

    private val apiService by lazy {
        RetrofitClient.getInstance().create(ApiService::class.java)
    }

    fun pushComment(
        accessToken: String,
        music: String,
        content: String,
        onDone: (String) -> Unit,
        onError: (String) -> Unit
    ) {
        executeTask(
            request = { pushNewComment(accessToken, music, content) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {
                onError.invoke("${it.message}")
            },
            showLoading = true
        )
    }

    private suspend fun pushNewComment(
        accessToken: String,
        music: String,
        content: String
    ): DataResult<String> {
        var data: String
        return suspendCoroutine { continuation ->
            val commentRequest = CommentRequest(music, content)
            val call = apiService.pushComment("Bearer $accessToken", commentRequest)
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data != null) {
                    data = response.body()?.message!!
                    continuation.resume(DataResult.Success(data))
                } else {
                    val errorBody = response.body()?.message
                    continuation.resume(DataResult.Error(Exception(errorBody)))
                }
            }, failed = { throwAble ->
                continuation.resume(DataResult.Error(Exception(throwAble.message ?: "Network error")))
            })
        }
    }

    fun getAllUser(
        accessToken: String,
        onDone: (MutableList<User>) -> Unit
    ) {
        executeTask(
            request = { getUsers(accessToken) },
            onSuccess = { result ->
                onDone.invoke(result)
            }, onError = {

            },
            showLoading = true
        )
    }

    private suspend fun getUsers(accessToken: String): DataResult<MutableList<User>> {
        var listUser = mutableListOf<User>()
        return suspendCoroutine { continuation ->
            val call = apiService.getAllUser("Bearer $accessToken")
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data != null) listUser =
                    response.body()?.data!!.results.toMutableList()
                continuation.resume(DataResult.Success(listUser))
            }, failed = {
                continuation.resume(DataResult.Success(listUser))
            })
        }
    }

    fun getCommentOfSong(
        accessToken: String,
        musicId: String,
        onDone: (MutableList<ListCommentOfSongResponse>) -> Unit
    ) {
        executeTask(
            request = { getCommentsOfSong(accessToken, musicId) },
            onSuccess = { result ->
                onDone.invoke(result)
            },
            onError = {

            },
            showLoading = true
        )
    }

    private suspend fun getCommentsOfSong(
        accessToken: String,
        musicId: String
    ): DataResult<MutableList<ListCommentOfSongResponse>> {
        var list = mutableListOf<ListCommentOfSongResponse>()
        return suspendCoroutine { continuation ->
            val call = apiService.getCommentOfSong("Bearer $accessToken", musicId)
            call.enqueueShort(success = { response ->
                if (response.isSuccessful && response.body()?.data != null) list =
                    response.body()?.data!!.comments.toMutableList()
                continuation.resume(DataResult.Success(list))
            }, failed = {
                continuation.resume(DataResult.Success(list))
            })
        }
    }
}