package com.example.musicapp.base.entity

import com.example.musicapp.entities.User

data class DataAllUser(
    val results:MutableList<User>
)
