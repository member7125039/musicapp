package com.example.musicapp.base.entity

data class SongWithCategoryResponse(
    val status: Int,
    val message: String,
    val data: DataSongWithCategoryResponse
)
