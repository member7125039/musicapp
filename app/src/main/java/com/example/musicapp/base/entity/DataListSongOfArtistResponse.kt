package com.example.musicapp.base.entity

import com.example.musicapp.entities.SongOfArtist

data class DataListSongOfArtistResponse(
    val singer: String,
    val musics: List<SongOfArtist>
)
