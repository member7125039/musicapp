package com.example.musicapp

import android.content.Intent
import android.view.LayoutInflater
import androidx.viewpager2.widget.ViewPager2
import com.example.musicapp.adapter.TabViewPager
import com.example.musicapp.adapter.ViewPagerAdapter
import com.example.musicapp.base.base_view.BaseActivity
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.handleBackPressed
import com.example.musicapp.base.utils.extension.showOrGone
import com.example.musicapp.databinding.ActivityMainBinding
import com.example.musicapp.screen.LogoutBottomSheetDialog
import com.example.musicapp.screen.login.LoginActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {

    //region variable
    private val dialog by lazy {
        AlertMessageDialog(this)
    }
    private val callBackViewPager = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            when (TabViewPager.getTabByPosition(position)) {
                TabViewPager.HOME -> binding.btnNav.menu.findItem(R.id.home).isChecked = true
                TabViewPager.EXPLORE -> binding.btnNav.menu.findItem(R.id.explore).isChecked =
                    true

                TabViewPager.LIBRARY -> binding.btnNav.menu.findItem(R.id.library).isChecked =
                    true

                TabViewPager.PROFILE -> binding.btnNav.menu.findItem(R.id.profile).isChecked =
                    true

            }
        }
    }
    //endregion

    override fun onResume() {
        super.onResume()
        binding.viewPager.registerOnPageChangeCallback(callBackViewPager)
    }

    override fun onPause() {
        super.onPause()
        binding.viewPager.unregisterOnPageChangeCallback(callBackViewPager)
    }

    override fun initView() {
        SharePrefUtils.init(this)
    }

    override fun initData() {
        binding.viewPager.adapter = ViewPagerAdapter(supportFragmentManager, lifecycle)
    }

    override fun initListener() {
        handleBackPressed {
            onBack()
        }
        binding.btnNav.setOnItemSelectedListener { item ->
            val tabToSelect: TabViewPager = when (item.itemId) {
                R.id.home -> TabViewPager.HOME
                R.id.explore -> TabViewPager.EXPLORE
                R.id.library -> TabViewPager.LIBRARY
                R.id.profile -> TabViewPager.PROFILE
                else -> return@setOnItemSelectedListener false
            }
            binding.viewPager.currentItem = tabToSelect.ordinal
            true
        }
    }

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    private fun onBack() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            //hien log out account va back signFragment
            showDialogLogout()
        } else {
            backFragment()
        }
    }

    fun backFragment() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            visibility(true)
            supportFragmentManager.popBackStack()
        } else {
            visibility(false)
            supportFragmentManager.popBackStack()
        }
    }

    fun showAndHideDialog(show: Boolean) {
        FunctionUtils.showAndLoadingPleaseWait(this, dialog, show)
    }

    private fun showDialogLogout() {
        val logoutBottomSheetDialog = LogoutBottomSheetDialog()
        logoutBottomSheetDialog.apply {
            show(supportFragmentManager, "")
            setOnClickListener(object :
                LogoutBottomSheetDialog.OnClickListener {
                override fun onYesClick() {
                    SharePrefUtils.saveKey(Constant.KEY_REMEMBER, false)
                    SharePrefUtils.saveKey(Constant.KEY_USER_NAME, "")
                    SharePrefUtils.saveKey(Constant.KEY_PASSWORD, "")
                    val intent = Intent(this@MainActivity, LoginActivity::class.java)
                    startActivity(intent)
                }

                override fun onNoClick() {
                    dismiss()
                }
            })
        }
    }

    fun visibility(isVisibility: Boolean) {
        binding.viewPager.showOrGone(isVisibility)
        binding.btnNav.showOrGone(isVisibility)
    }
}