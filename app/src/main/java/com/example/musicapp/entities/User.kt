package com.example.musicapp.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class User(
    @SerializedName("username") var username: String? = null,
    @SerializedName("fullname") var fullname: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("avatar") var avatar: String? = null,
    @SerializedName("dateOfBirth") var dateOfBirth: String? = null,
    @SerializedName("gender") var gender: String? = null,
    @SerializedName("role") var role: String? = null,
    @SerializedName("activeVip") var activeVip: Boolean? = null,
    @SerializedName("id") var id: String? = null,

    ) : Serializable {
    fun getUrlAvatar() = "https://hitmusic.tech$avatar"
}
