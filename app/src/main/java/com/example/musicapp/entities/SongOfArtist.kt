package com.example.musicapp.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SongOfArtist(
    @SerializedName("title") var title: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("length") var length: String? = null,
    @SerializedName("id") var id: String? = null,
) : Serializable
