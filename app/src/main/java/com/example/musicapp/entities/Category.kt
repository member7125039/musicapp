package com.example.musicapp.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Category(
    @SerializedName("name") var name: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("createdAt") var createdAt: String? = null,
    @SerializedName("id") var id: String? = null,
) : Serializable
