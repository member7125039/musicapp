package com.example.musicapp.screen.sign_up

import android.content.Intent
import android.graphics.Color
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.entity.LoginRequest
import com.example.musicapp.base.entity.RegisterRequest
import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.enqueueShort
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.databinding.FragmentSignUpBinding
import com.example.musicapp.screen.login.LoginActivity
import com.example.musicapp.screen.login.SignInFragment

class SignUpFragment : BaseFragment<FragmentSignUpBinding>() {
    private lateinit var diaLog: AlertMessageDialog
    override fun initListener() {

        binding.backSignUp.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
        var rememberMe = false
        binding.rememberMe.setOnClickListener {
            rememberMe = if (rememberMe) {
                binding.rememberMe.setColorFilter(Color.parseColor("#FFFFFFFF"))
                false

            } else {
                binding.rememberMe.setColorFilter(Color.parseColor("#06C149"))
                true
            }
        }
        binding.tvSignIn.setOnClickListener {
            (activity as LoginActivity).replaceFragment(
                R.id.login, SignInFragment(), true, null
            )
        }
        var hidePassword = true
        binding.hidePassword.setOnClickListener {
            //ko hien password
            if (hidePassword) {
                //show
                binding.hidePassword.setImageResource(R.drawable.show_password)
                binding.password.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
                hidePassword = false
            } else {
                hidePassword = true
                binding.hidePassword.setImageResource(R.drawable.hide_pass)
                binding.password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
        var hideConfirmPassword = true
        binding.hideConfirmPassword.setOnClickListener {
            if (hideConfirmPassword) {
                hideConfirmPassword = false
                binding.hideConfirmPassword.setImageResource(R.drawable.show_password)
                binding.confirmPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()

            } else {
                hideConfirmPassword = true
                binding.hideConfirmPassword.setImageResource(R.drawable.hide_pass)
                binding.confirmPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }

        }
        binding.signUp.setOnClickListener {
            signUp()
        }
    }

    private fun signUp() {
        val username: String = binding.userName.text.toString()
        val fullName: String = binding.fullName.text.toString()
        val email: String = binding.email.text.toString()
        val password: String = binding.password.text.toString()
        val passwordAgain: String = binding.confirmPassword.text.toString()
        if (username == "") {
            requireContext().toast(R.string.username_cannot_be_left_blank.toString())
        }
        if (fullName == "") {
            requireContext().toast(R.string.fullname_cannot_be_left_blank.toString())
        }
        if (email == "") {
            requireContext().toast(R.string.email_cannot_be_left_blank.toString())
        }
        if (password == "") {
            requireContext().toast(R.string.password_cannot_be_left_blank.toString())
        }
        if (passwordAgain == "") {
            requireContext().toast(R.string.password_again_cannot_be_left_blank.toString())
        }
        if (password != passwordAgain) {
            requireContext().toast(R.string.password_and_password_again_must_be_same.toString())
        }
        if (username != "" && fullName != "" && email != "" && password != "" && passwordAgain != "" && password == passwordAgain) {
            diaLog.show(
                getString(R.string.notification),
                getString(R.string.registering_please_wait),
                "",
                ""
            )
            register(fullName, username, email, password, passwordAgain)
        }
    }

    private fun register(
        fullName: String, username: String, email: String, password: String, passwordAgain: String
    ) {
        val apiService = RetrofitClient.getInstance().create(ApiService::class.java)
        val registerRequest =
            RegisterRequest(fullName, username, email, password, passwordAgain)
        val call = apiService.register(registerRequest)
        call.enqueueShort(success = { response ->
            diaLog.hide()
            if (response.isSuccessful) {
                login(username, password, apiService)
            }
        }, failed = { throwAble ->
            diaLog.hide()
            requireContext().toast("${throwAble.message}")
        })
    }

    private fun login(username: String, password: String, apiService: ApiService) {
        val loginRequest = LoginRequest(username, password)
        val call2 = apiService.login(loginRequest)
        call2.enqueueShort(success = { response2 ->
            if (response2.isSuccessful) {
                val loginResponse = response2.body()
                if (loginResponse != null) {
                    val accessToken = loginResponse.accessToken
                    SharePrefUtils.saveKey(Constant.ACCESS_TOKEN, accessToken)
                }
                val intent = Intent(requireContext(), MainActivity::class.java)
                startActivity(intent)
            } else {
                requireContext().toast(getString(R.string.username_or_password_is_incorrect))
            }
        }, failed = { t ->
            diaLog.hide()
            requireContext().toast("${t.message}")
        })
    }

    override fun initData() {

    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        diaLog = AlertMessageDialog(requireContext())
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSignUpBinding {
        return FragmentSignUpBinding.inflate(inflater, container, false)
    }
}