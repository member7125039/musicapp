package com.example.musicapp.screen.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.ArtistAdapter
import com.example.musicapp.adapter.CategoryAdapter
import com.example.musicapp.adapter.SongAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.*
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.viewmodel.HomeViewModel
import com.example.musicapp.databinding.FragmentHomeBinding
import com.example.musicapp.entities.Artist
import com.example.musicapp.entities.Category
import com.example.musicapp.entities.Song
import com.example.musicapp.screen.play.PlayingFragment
import java.util.*

class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[HomeViewModel::class.java]
    }
    private val loadingData by lazy {
        AlertMessageDialog(requireActivity())
    }
    private val categoryAdapter by lazy {
        CategoryAdapter()
    }
    private val songAdapter by lazy {
        SongAdapter()
    }
    private val artistAdapter by lazy {
        ArtistAdapter()
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }

    override fun initListener() {
        val mainActivity = requireActivity() as MainActivity
        binding.seeAllSongs.setOnClickListener {
            clickSeeAllSong(mainActivity)
        }
        songAdapter.setOnClickItem { item, position ->
            clickSong(mainActivity, position, item)
        }
        artistAdapter.setOnClickItem { item, _ ->
            clickArtist(item)
        }
        binding.seeAllArtist.setOnClickListener {
            clickSeeAllArtist(mainActivity)
        }
        categoryAdapter.setOnClickItem { item, _ ->
            clickCategory(item)
        }
    }

    private fun clickSeeAllSong(mainActivity: MainActivity) {
        mainActivity.replaceFragment(
            R.id.main, AllSongFragment(), true, null
        )
        mainActivity.visibility(false)
    }

    private fun clickCategory(item: Category?) {
        SharePrefUtils.saveKey(Constant.CATEGORYID, item?.id)
        viewModel.getDataSongWithCategory(
            requireContext(),
            accessToken,
            item?.id!!,
            onDone = { data ->
                Constant.listSongOfCategory = data
                (requireActivity() as MainActivity).replaceFragment(
                    R.id.main, SongOfCategoryFragment(), true, null
                )
                (requireActivity() as MainActivity).visibility(false)
            },
            onError = {
                requireContext().toast(it)
            })
    }

    private fun clickSeeAllArtist(mainActivity: MainActivity) {
        mainActivity.replaceFragment(
            R.id.main, AllArtistFragment(), true, null
        )
        mainActivity.visibility(false)
    }

    private fun clickArtist(item: Artist?) {
        SharePrefUtils.saveKey(Constant.SINGERID, item?.id)
        SharePrefUtils.saveKey(Constant.NAMEARTIST, item?.fullname)
        val bundle = pushBundle(Constant.ARTIST, item)
        (activity as MainActivity).apply {
            replaceFragment(R.id.main, ArtistIsSongFragment(), true, bundle)
            visibility(false)
        }
    }

    private fun clickSong(mainActivity: MainActivity, position: Int, item: Song?) {
        mainActivity.showAndHideDialog(true)
        if (requireActivity().isInternetAvailable()) {
            SharePrefUtils.saveKey(Constant.POSITION, position)
            val bundle = pushBundle(Constant.ITEM, item)
            val playingFragment = PlayingFragment().apply {
                arguments = bundle
            }
            mainActivity.apply {
                replaceFragment(R.id.main, playingFragment, true, null)
                visibility(false)
            }
        } else {
            requireContext().toast(getString(R.string.error_network))
        }
    }

    override fun initData() {

    }

    override fun initView() {
        var sumApi = 4
        FunctionUtils.showAndLoadingPleaseWait(requireActivity(), loadingData, true)
        viewModel.getProfile(accessToken, onDone = {
            Constant.profile = it
            binding.fullName.text = it.fullname
            sumApi--
            hideLoadingData(sumApi)
        }, onError = {
            requireContext().toast(it)
        })
        viewModel.getCategory(accessToken, onDone = { data ->
            categoryAdapter.setDataList(data)
            Constant.listCategory = data
            sumApi--
            hideLoadingData(sumApi)
        }, onError = {
            requireContext().toast(it)
        })
        viewModel.getArtistAtHome(accessToken, onDone = {
            artistAdapter.setDataList(it)
            Constant.listArtist = it
            sumApi--
            hideLoadingData(sumApi)
        }, onError = {
            requireContext().toast(it)
        })
        viewModel.getSongAtHome(accessToken, onDone = {
            songAdapter.setDataList(it)
            Constant.listSong = it
            sumApi--
            hideLoadingData(sumApi)
        }, onError = {
            requireContext().toast(it)
        })
        SharePrefUtils.init(requireContext())

        binding.hello.text = getHelloUser()
        binding.listSong.setLinearLayoutManager(requireContext(), songAdapter,RecyclerView.HORIZONTAL)
        binding.listArtist.setLinearLayoutManager(requireContext(), artistAdapter,RecyclerView.HORIZONTAL)
        binding.listCategory.setGridManager(requireContext(), 2, categoryAdapter)
    }

    private fun hideLoadingData(sumApi: Int) {
        if (sumApi == 0) FunctionUtils.showAndLoadingPleaseWait(
            requireContext(), loadingData, false
        )
    }

    private fun getHelloUser(): String {
        val currentTime = Calendar.getInstance()
        return when (currentTime.get(Calendar.HOUR_OF_DAY)) {
            in 0..11 -> "Good Morning"
            in 12..16 -> "Good Afternoon"
            else -> "Good Evening"
        }
    }

    override fun inflateLayout(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(inflater, container, false)
    }
}