package com.example.musicapp.screen.login

import android.view.LayoutInflater
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseActivity
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.entity.LoginRequest
import com.example.musicapp.base.entity.LoginResponse
import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.enqueueShort
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.ExtensionFunction.toastLong
import com.example.musicapp.base.utils.extension.handleBackPressed
import com.example.musicapp.base.utils.extension.openActivity
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.databinding.ActivityLoginBinding
import com.example.musicapp.screen.ExitAppBottomSheetDialog
import retrofit2.Response

class LoginActivity : BaseActivity<ActivityLoginBinding>() {
    //region variable
    companion object {
        const val REMEMBER_KEY = "isRemember"
        const val USER_NAME_KEY = "userName"
        const val PASSWORD_KEY = "password"
    }

    private val dialog: AlertMessageDialog by lazy {
        AlertMessageDialog(this)
    }
    //endregion

    override fun initView() {
        SharePrefUtils.init(this)
        val isRemember: Boolean = SharePrefUtils.getBoolean(REMEMBER_KEY)
        if (isRemember) {
            //auto login
            autoLogin()
        } else {
            replaceFragment(R.id.login, LayoutLoginFragment(), true, null)
        }
    }

    override fun initData() {
    }

    override fun initListener() {
        handleBackPressed {
            onBack()
        }
    }

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ActivityLoginBinding {
        return ActivityLoginBinding.inflate(layoutInflater)
    }

    private fun autoLogin() {
        dialog.show(
            getString(R.string.notification),
            getString(R.string.logging_in_please_wait)
        )
        val userName = SharePrefUtils.getString(USER_NAME_KEY)
        val password = SharePrefUtils.getString(PASSWORD_KEY)
        //TODO: check lai logic
        if (userName == "" || password == "") {
            dialog.hide()
            toast(getString(R.string.login_failed))
            replaceFragment(R.id.login, LayoutLoginFragment(), true, null)
        } else {
            callAPiLogin(userName, password)
        }
    }

    private fun callAPiLogin(userName: String, password: String) {
        val apiService = RetrofitClient.getInstance().create(ApiService::class.java)
        val loginRequest = LoginRequest(userName, password)
        val call = apiService.login(loginRequest)
        call.enqueueShort(success = { response ->
            if (response.isSuccessful) {
                goToMain(response)
            } else {
                dialog.hide()
                this@LoginActivity.toast(getString(R.string.username_or_password_is_incorrect))
            }
        }, failed = {
            dialog.hide()
            this@LoginActivity.toastLong("${it.message}")
            replaceFragment(R.id.login, SignInFragment(), true, null)
        })
    }

    private fun goToMain(response: Response<LoginResponse>) {
        dialog.hide()
        val loginResponse = response.body()
        loginResponse?.let {
            SharePrefUtils.saveKey(Constant.ACCESS_TOKEN, it.accessToken)
        }
        //TODO: login xong có finish màn login này không?, check lại logic chỗ này
        openActivity(MainActivity::class.java, isFinish = false)
    }

    private fun onBack() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            //bottom logout
            showDialogExitApp()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    private fun showDialogExitApp() {
        val exitAppBottomSheetDialog = ExitAppBottomSheetDialog()
        exitAppBottomSheetDialog.apply {
            show(supportFragmentManager, "")
            setOnClickListener(object :
                ExitAppBottomSheetDialog.OnClickListener {
                override fun onYesClick() {
                    finishAffinity()
                }

                override fun onCancelClick() {
                    dismiss()
                }
            })
        }
    }
}