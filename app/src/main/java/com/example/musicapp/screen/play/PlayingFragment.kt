package com.example.musicapp.screen.play


import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.lifecycle.ViewModelProvider
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.getDataSerializable
import com.example.musicapp.base.utils.extension.setOnProgressChange
import com.example.musicapp.base.utils.timerConvert
import com.example.musicapp.base.viewmodel.PlayingViewModel
import com.example.musicapp.databinding.FragmentPlayingBinding
import com.example.musicapp.databinding.PopupMenuLayoutBinding
import com.example.musicapp.entities.Song
import com.example.musicapp.screen.bottomSheet.CommentOfSongBottomSheetDialog

class PlayingFragment : BaseFragment<FragmentPlayingBinding>() {
    //region variable
    companion object {
        const val ONE_SECOND = 1000
    }

    private var apiSuccess = false
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[PlayingViewModel::class.java]
    }
    private val commentOfSongBottomSheetDialog by lazy {
        CommentOfSongBottomSheetDialog()
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }
    val mediaPlayer = MediaPlayer()
    val handler = Handler(Looper.myLooper()!!)
    private var position = 0
    private var isPlay: Boolean = true
    private val handlerThread = HandlerThread("SeekBarUpdateThread")
    private lateinit var backgroundHandler: Handler
    //endregion

    override fun initListener() {
        binding.backPlayingMusic.setOnClickListener {
            (requireActivity() as MainActivity).backFragment()
        }
        binding.next.setOnClickListener {
            backAndNextMusic(true)
        }
        binding.back.setOnClickListener {
            backAndNextMusic(false)
        }
        binding.play.setOnClickListener {
            playPauseMusic()
        }
        binding.back10s.setOnClickListener {
            backOrNext10s(false)
        }
        binding.next10s.setOnClickListener {
            backOrNext10s(true)
        }
        val diaLogBinding = PopupMenuLayoutBinding.inflate(LayoutInflater.from(requireContext()))
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val window = PopupWindow(diaLogBinding.root, width, height, false)
        window.apply {
            isOutsideTouchable = true
            isFocusable = true
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            contentView = layoutInflater.inflate(R.layout.popup_menu_layout, null)
        }
        var isPopupVisible = false
        binding.moreMusic.setOnClickListener {
            isPopupVisible = if (!isPopupVisible) {
                window.showAsDropDown(binding.moreMusic, -285, 5)
                true
            } else {
                window.dismiss()
                false
            }
        }
        window.setOnDismissListener {
            isPopupVisible = false
        }
        binding.comment.setOnClickListener {
            if (apiSuccess && Constant.users.size != 0) {
                showComments()
            }
        }
    }

    private fun showComments() {
        commentOfSongBottomSheetDialog.show(childFragmentManager, "")
    }

    private fun playPauseMusic() {
        isPlay = !isPlay
        if (isPlay) {
            mediaPlayer.start()
            binding.play.setImageResource(R.drawable.pause)
        } else {
            binding.play.setImageResource(R.drawable.play)
            mediaPlayer.pause()
        }
    }

    private fun backOrNext10s(isNext: Boolean) {
        val currentPosition = mediaPlayer.currentPosition
        val newPosition = currentPosition + 10 * ONE_SECOND * if (isNext) 1 else -1
        if (isNext) {
            if (newPosition > mediaPlayer.duration) {
                backAndNextMusic(true)
                commentOfSongBottomSheetDialog.dismiss()
            } else {
                mediaPlayer.seekTo(newPosition)
            }
        } else {
            if (newPosition < 0) {
                mediaPlayer.seekTo(0)
            } else {
                mediaPlayer.seekTo(newPosition)
            }
        }
        binding.seekBar.progress = mediaPlayer.currentPosition
        binding.currentTime.text = (mediaPlayer.currentPosition / ONE_SECOND).timerConvert()
    }

    private fun backAndNextMusic(isNext: Boolean) {
        binding.play.setImageResource(R.drawable.pause)
        position += if (isNext) 1 else -1
        val item: Song = if (position > Constant.listSong.size - 1) {
            position = 0
            Constant.listSong[0]
        } else {
            if (position < 0) {
                position = Constant.listSong.size - 1
                Constant.listSong[position]
            } else {
                Constant.listSong[position]
            }
        }
        initViewOfMusic(item)
    }

    override fun initData() {
        arguments?.getDataSerializable(Constant.ITEM, Song::class.java)?.let {
            initViewOfMusic(it)
            if (Constant.users.size == 0) {
                viewModel.getAllUser(accessToken, onDone = { data ->
                    Constant.users = data
                })
            }
        }
        SharePrefUtils.init(requireContext())
        val songId = SharePrefUtils.getString(Constant.ID)
        val lastIndex = Constant.listSong.indexOfLast { it.id == songId }
        if (lastIndex != -1) {
            position = lastIndex
        }
    }

    private fun initViewOfMusic(item: Song?) {
        /**adding placeHolder and error*/
        SharePrefUtils.saveKey(Constant.IDSONGCOMMENT, item?.id)
        viewModel.getCommentOfSong(accessToken, item?.id!!, onDone = { data ->
            Constant.comments = data
            binding.sumComments.text = data.size.toString()
            apiSuccess = true
            (activity as MainActivity).showAndHideDialog(false)
        })
        FunctionUtils.loadImage(requireContext(), binding.avatar, item.image, 25)
        commentOfSongBottomSheetDialog.setOnDismissListener {
            binding.sumComments.text = Constant.comments.size.toString()
        }
        binding.nameMusic.text = item.title
        binding.description.text = item.description
        binding.maxSeekbar.text = item.length?.timerConvert()

        mediaPlayer.apply {
            reset()
            setDataSource(item.audio)
            prepare()
            start()
            setOnCompletionListener {
                backAndNextMusic(true)
                commentOfSongBottomSheetDialog.dismiss()
            }
        }
        binding.seekBar.setOnProgressChange { seekBar, progress, fromUser ->
            if (fromUser) {
                mediaPlayer.seekTo(progress)
            }
        }
        updateSeekBar()
    }

    private fun initHandlerThread() {
        handlerThread.start()
        backgroundHandler = Handler(handlerThread.looper)
    }

    private fun releaseHandlerThread() {
        handlerThread.quitSafely()
    }

    private fun updateSeekBar() {
        binding.seekBar.max = mediaPlayer.duration
        val runnable = object : Runnable {
            override fun run() {
                binding.seekBar.progress = mediaPlayer.currentPosition
                binding.currentTime.text = (mediaPlayer.currentPosition / ONE_SECOND).timerConvert()
                handler.postDelayed(this, ONE_SECOND.toLong())
            }
        }
        handler.postDelayed(runnable, ONE_SECOND.toLong())
    }

    override fun onDestroy() {
        mediaPlayer.release()
        handler.removeCallbacksAndMessages(null)
        releaseHandlerThread()
        super.onDestroy()
    }

    override fun initView() {
        initHandlerThread()
        SharePrefUtils.init(requireContext())
        SharePrefUtils.getInt(Constant.POSITION)
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentPlayingBinding {
        return FragmentPlayingBinding.inflate(inflater, container, false)
    }
}