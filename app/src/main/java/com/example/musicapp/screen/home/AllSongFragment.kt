package com.example.musicapp.screen.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.AllSongAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.pushBundle
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.base.utils.extension.setGridManager
import com.example.musicapp.base.viewmodel.HomeViewModel
import com.example.musicapp.databinding.FragmentAllSongBinding
import com.example.musicapp.screen.play.PlayingFragment

class AllSongFragment : BaseFragment<FragmentAllSongBinding>() {
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[HomeViewModel::class.java]
    }
    private val allSongAdapter by lazy {
        AllSongAdapter()
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }

    override fun initListener() {
        binding.backAllSong.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
            (requireActivity() as MainActivity).visibility(true)
        }
        allSongAdapter.setOnClickItem { item, position ->
            (activity as MainActivity).showAndHideDialog(true)
            SharePrefUtils.saveKey(Constant.ID, item?.id)
            SharePrefUtils.saveKey(Constant.POSITION, position)
            val bundle = pushBundle(Constant.ITEM, item)
            (activity as MainActivity).replaceFragment(R.id.main, PlayingFragment(), true, bundle)
        }
    }

    override fun initData() {
        allSongAdapter.setDataList(Constant.listSong)
    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        binding.listSong.setGridManager(requireContext(), 2, allSongAdapter)
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAllSongBinding {
        return FragmentAllSongBinding.inflate(inflater, container, false)
    }
}