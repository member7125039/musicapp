package com.example.musicapp.screen.library


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.CategoryInLibraryAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.base.utils.extension.setLinearLayoutManager
import com.example.musicapp.base.utils.extension.setLinearLayoutManagerVertical
import com.example.musicapp.base.viewmodel.HomeViewModel
import com.example.musicapp.databinding.FragmentCategoryInLibraryBinding
import com.example.musicapp.entities.Category
import com.example.musicapp.screen.home.SongOfCategoryFragment


class CategoryInLibraryFragment : BaseFragment<FragmentCategoryInLibraryBinding>() {
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[HomeViewModel::class.java]
    }
    private val categoryInLibraryAdapter by lazy {
        CategoryInLibraryAdapter()
    }

    override fun initListener() {
        binding.backCategory.setOnClickListener {
            (requireActivity() as MainActivity).backFragment()
        }
        categoryInLibraryAdapter.setOnClickItem { item, _ ->
            SharePrefUtils.saveKey(Constant.CATEGORYID, item?.id)
            getDataSongWithCategory(item)
        }
    }

    private fun getDataSongWithCategory(item: Category?) {
        viewModel.getDataSongWithCategory(
            requireContext(),
            accessToken,
            item?.id!!,
            onDone = { data ->
                Constant.listSongOfCategory = data
                (requireActivity() as MainActivity).replaceFragment(
                    R.id.main,
                    SongOfCategoryFragment(),
                    true,
                    null
                )
                (requireActivity() as MainActivity).visibility(false)
            }, onError = {
                requireContext().toast(it)
            })
    }

    override fun initData() {
        categoryInLibraryAdapter.setDataList(Constant.listCategory)
    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        binding.listCategory.setLinearLayoutManager(requireContext(), categoryInLibraryAdapter,RecyclerView.VERTICAL)
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentCategoryInLibraryBinding {
        return FragmentCategoryInLibraryBinding.inflate(inflater, container, false)
    }
}