package com.example.musicapp.screen.bottomSheet

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.R
import com.example.musicapp.adapter.CommentsOfSongAdapter
import com.example.musicapp.base.base_view.BaseBottomSheetDialog
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.gone
import com.example.musicapp.base.utils.extension.show
import com.example.musicapp.base.viewmodel.PlayingViewModel
import com.example.musicapp.databinding.BottomCommentOfSongBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior

class CommentOfSongBottomSheetDialog : BaseBottomSheetDialog<BottomCommentOfSongBinding>() {

    private val commentsOfSongAdapter by lazy {
        CommentsOfSongAdapter()
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }
    private val idOfSong by lazy {
        SharePrefUtils.getString(Constant.IDSONGCOMMENT)
    }
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[PlayingViewModel::class.java]
    }

    private val commentOfSongBottomSheetDialog by lazy {
        CommentNewOfSongBottomSheetDialog()
    }

    private var onDismissListener: (() -> Unit)? = null

    override fun initListener() {
        binding.closeComments.setOnClickListener {
            dismiss()
        }
        binding.newComment.setOnClickListener {
            commentOfSongBottomSheetDialog.show(childFragmentManager, "")
            commentOfSongBottomSheetDialog.setOnDismissListener {
                viewModel.getCommentOfSong(accessToken, idOfSong, onDone = { data ->
                    Constant.comments = data
                    initRecyclerView()
                })
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val bottomSheet =
                dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            val behavior = BottomSheetBehavior.from(bottomSheet)
            val screenHeight = resources.displayMetrics.heightPixels
            val halfScreenHeight = if (Constant.comments.size > 2) {
                2 * screenHeight / 3
            } else {
                screenHeight / 2
            }
            bottomSheet.layoutParams.height = halfScreenHeight
            behavior.peekHeight = halfScreenHeight
        }
    }

    fun setOnDismissListener(listener: (() -> Unit)?) {
        this.onDismissListener = listener
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }

    override fun initData() {
        SharePrefUtils.init(requireContext())
        initRecyclerView()
    }

    private fun initRecyclerView() {
        if (Constant.comments.size != 0) {
            binding.noComments.gone()
            commentsOfSongAdapter.setDataList(Constant.comments)
            configureRecyclerView(
                binding.comments,
                commentsOfSongAdapter,
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            )
            if (Constant.newComment) {
                requireContext().toast(getString(R.string.there_is_1_new_comment))
                !Constant.newComment
            }
        } else {
            binding.noComments.show()
        }
    }

    private fun configureRecyclerView(
        recyclerView: RecyclerView,
        adapter: RecyclerView.Adapter<*>,
        layoutManager: RecyclerView.LayoutManager
    ) {
        recyclerView.apply {
            this.adapter = adapter
            this.layoutManager = layoutManager
        }
    }

    override fun initView() {
        FunctionUtils.loadImage(
            requireContext(),
            binding.avatarAccount,
            Constant.profile.avatar,
            25
        )
    }

    override fun inflateBinding(layoutInflater: LayoutInflater): BottomCommentOfSongBinding {
        return BottomCommentOfSongBinding.inflate(layoutInflater)
    }
}