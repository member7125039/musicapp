package com.example.musicapp.screen.library

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.SongAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.pushBundle
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.base.utils.extension.setLinearLayoutManager
import com.example.musicapp.base.utils.extension.setLinearLayoutManagerHorizontal
import com.example.musicapp.databinding.FragmentLibraryBinding
import com.example.musicapp.entities.Song
import com.example.musicapp.screen.home.AllArtistFragment
import com.example.musicapp.screen.play.PlayingFragment


class LibraryFragment : BaseFragment<FragmentLibraryBinding>() {
    private val songAdapter by lazy {
        SongAdapter()
    }

    override fun initListener() {
        val mainActivity = requireActivity() as MainActivity
        binding.seeAllSongs.setOnClickListener {
            mainActivity.replaceFragment(
                R.id.main, AllSongInLibraryFragment(), true, null
            )
            mainActivity.visibility(false)
        }
        songAdapter.setOnClickItem { item, position ->
            clickSong(position, item, mainActivity)
        }
        binding.moreArtist.setOnClickListener {
            mainActivity.replaceFragment(
                R.id.main, AllArtistFragment(), true, null
            )
            mainActivity.visibility(false)
        }
        binding.morePlaylists.setOnClickListener {
            mainActivity.replaceFragment(
                R.id.main, CategoryInLibraryFragment(), true, null
            )
            mainActivity.visibility(false)
        }
    }

    private fun clickSong(position: Int, item: Song?, mainActivity: MainActivity) {
        SharePrefUtils.saveKey(Constant.POSITION, position)
        val bundle = pushBundle(Constant.ITEM, item)
        val playingFragment = PlayingFragment().apply {
            arguments = bundle
        }
        mainActivity.apply {
            replaceFragment(R.id.main, playingFragment, true, null)
            visibility(false)
        }
    }

    override fun initData() {
        SharePrefUtils.init(requireContext())
        songAdapter.setDataList(Constant.listSong)
    }

    override fun initView() {
        binding.listSong.setLinearLayoutManager(requireContext(), songAdapter,RecyclerView.HORIZONTAL)
    }

    override fun inflateLayout(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentLibraryBinding {
        return FragmentLibraryBinding.inflate(inflater, container, false)
    }
}