package com.example.musicapp.screen.bottomSheet


import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseBottomSheetDialog
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.clear
import com.example.musicapp.base.viewmodel.PlayingViewModel
import com.example.musicapp.databinding.BottomCommentNewOfSongBinding

class CommentNewOfSongBottomSheetDialog : BaseBottomSheetDialog<BottomCommentNewOfSongBinding>() {
    //region variable
    private var checkCommentNull = false
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[PlayingViewModel::class.java]
    }
    private val diaLog by lazy {
        AlertMessageDialog(requireActivity())
    }
    private var onDismissListener: (() -> Unit)? = null
    //endregion

    override fun initListener() {
        binding.newComment.doOnTextChanged { text, _, _, _ ->
            checkCommentNull = if (text?.isNotEmpty() == true) {
                binding.pushNewComment.setImageResource(R.drawable.submit)
                true
            } else {
                binding.pushNewComment.setImageResource(R.drawable.submit0)
                false
            }
        }
        binding.pushNewComment.setOnClickListener {
            if (checkCommentNull) {
                diaLog.show(
                    getString(R.string.notification),
                    getString(R.string.please_wait),
                    "",
                    ""
                )
                pushComment()
            }
        }
    }

    fun setOnDismissListener(listener: (() -> Unit)?) {
        this.onDismissListener = listener
    }

    private fun pushComment() {
        SharePrefUtils.init(requireContext())
        val id = SharePrefUtils.getString(Constant.IDSONGCOMMENT)
        val content = binding.newComment.text.toString()
        val accessToken = SharePrefUtils.getString(Constant.ACCESS_TOKEN)
        viewModel.pushComment(accessToken, id, content, onDone = { data ->
            diaLog.hide()
            requireContext().toast(data)
            Constant.newComment = true
            dismiss()
            binding.newComment.clear()
        }, onError = {
            diaLog.hide()
            requireContext().toast(it)
        })
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }

    override fun initData() {
    }

    override fun initView() {
        binding.nameAccount.text = Constant.profile.fullname
        val myString = "@${Constant.profile.username}"
        binding.userName.text = myString
        FunctionUtils.loadImage(requireContext(), binding.avtAccount, Constant.profile.avatar, 25)
    }

    override fun inflateBinding(layoutInflater: LayoutInflater): BottomCommentNewOfSongBinding {
        return BottomCommentNewOfSongBinding.inflate(layoutInflater)
    }
}