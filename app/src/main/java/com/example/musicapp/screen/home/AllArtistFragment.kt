package com.example.musicapp.screen.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.AllArtistAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.pushBundle
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.base.utils.extension.setGridManager
import com.example.musicapp.base.viewmodel.HomeViewModel
import com.example.musicapp.databinding.FragmentAllArtistBinding


class AllArtistFragment : BaseFragment<FragmentAllArtistBinding>() {

    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[HomeViewModel::class.java]
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }
    private val allArtistAdapter by lazy {
        AllArtistAdapter()
    }

    override fun initListener() {
        binding.backAllArtist.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
            (requireActivity() as MainActivity).visibility(true)
        }
        allArtistAdapter.setOnClickItem { item, _ ->
            (activity as MainActivity).showAndHideDialog(true)
            SharePrefUtils.saveKey(Constant.SINGERID, item?.id)
            SharePrefUtils.saveKey(Constant.NAMEARTIST, item?.fullname)
            val bundle = pushBundle(Constant.ARTIST, item)
            (activity as MainActivity).replaceFragment(
                R.id.main,
                ArtistIsSongFragment(),
                true,
                bundle
            )
        }
    }

    override fun initData() {
        allArtistAdapter.setDataList(Constant.listArtist)
    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        binding.listArtist.setGridManager(requireContext(), 2, allArtistAdapter)
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAllArtistBinding {
        return FragmentAllArtistBinding.inflate(inflater, container, false)
    }
}