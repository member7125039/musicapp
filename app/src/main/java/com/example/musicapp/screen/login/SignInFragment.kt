package com.example.musicapp.screen.login


import android.content.Intent
import android.graphics.Color
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.dialog.AlertMessageDialog
import com.example.musicapp.base.entity.LoginRequest
import com.example.musicapp.base.retrofit.ApiService
import com.example.musicapp.base.retrofit.RetrofitClient
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.enqueueShort
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.ExtensionFunction.toastLong
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.databinding.FragmentSignInBinding
import com.example.musicapp.screen.forgot.ForgotPasswordFragment
import com.example.musicapp.screen.sign_up.SignUpFragment


class SignInFragment : BaseFragment<FragmentSignInBinding>() {
    private var rememberMe = false
    private lateinit var dialog: AlertMessageDialog
    override fun initListener() {
        binding.backSignIn.setOnClickListener {
            requireActivity().supportFragmentManager.popBackStack()
        }
        binding.tvSignUp.setOnClickListener {
            (activity as LoginActivity).replaceFragment(
                R.id.login, SignUpFragment(), true, null
            )
        }
        binding.goForgotPassword.setOnClickListener {
            (activity as LoginActivity).replaceFragment(
                R.id.login, ForgotPasswordFragment(), true, null
            )
        }
        var hidePassword = true
        binding.hidePassword.setOnClickListener {
            //ko hien password
            if (hidePassword) {
                //show
                binding.hidePassword.setImageResource(R.drawable.show_password)
                binding.password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                hidePassword = false
            } else {
                hidePassword = true
                binding.hidePassword.setImageResource(R.drawable.hide_pass)
                binding.password.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }

        binding.rememberMe.setOnClickListener {
            rememberMe = if (rememberMe) {
                binding.rememberMe.setColorFilter(Color.parseColor("#FFFFFFFF"))
                false

            } else {
                binding.rememberMe.setColorFilter(Color.parseColor("#06C149"))
                true
            }
        }
        binding.LoginAccount.setOnClickListener {
            val user = binding.userName.text.toString()
            val password = binding.password.text.toString()
            login(user, password)
        }
    }

    private fun login(user: String, password: String) {
        if (user == "") {
            requireContext().toast(R.string.username_cannot_be_left_blank.toString())
            return
        }
        if (password == "") {
            requireContext().toast(R.string.password_cannot_be_left_blank.toString())
            return
        }
        dialog.show(
            getString(R.string.notification), getString(R.string.logging_in_please_wait), "", ""
        )

        val apiService = RetrofitClient.getInstance().create(ApiService::class.java)
        val loginRequest = LoginRequest(user, password)
        val call = apiService.login(loginRequest)
        call.enqueueShort(success = { response ->
            dialog.hide()
            if (response.isSuccessful) {
                val loginResponse = response.body()
                loginResponse?.let {
                    val accessToken = loginResponse.accessToken
                    SharePrefUtils.saveKey(Constant.ACCESS_TOKEN, accessToken)
                    SharePrefUtils.saveKey(Constant.ISREMEMBER, rememberMe)
                    SharePrefUtils.saveKey(Constant.USERNAME, user)
                    SharePrefUtils.saveKey(Constant.PASSWORD, password)
                }
                startMain()
            } else {
                requireContext().toast(getString(R.string.username_or_password_is_incorrect))
            }
        }, failed = { throwAble ->
            dialog.hide()
            requireContext().toastLong("${throwAble.message}")
        })
    }

    private fun startMain() {
        val intent = Intent(requireContext(), MainActivity::class.java)
        startActivity(intent)
    }

    override fun initData() {
        binding.userName.setText(SharePrefUtils.getString(Constant.KEY_USER_NAME))
        binding.password.setText(SharePrefUtils.getString(Constant.KEY_PASSWORD))
    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        dialog = AlertMessageDialog(requireActivity())
    }

    override fun inflateLayout(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentSignInBinding {
        return FragmentSignInBinding.inflate(inflater, container, false)
    }
}