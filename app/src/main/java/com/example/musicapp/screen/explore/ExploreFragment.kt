package com.example.musicapp.screen.explore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.AllArtistAdapter
import com.example.musicapp.adapter.CategoryInLibraryAdapter
import com.example.musicapp.adapter.SongInLibraryAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.entity.Category1
import com.example.musicapp.base.entity.Music
import com.example.musicapp.base.entity.SearchResponse
import com.example.musicapp.base.entity.Singer
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.ExtensionFunction.toastLong
import com.example.musicapp.base.utils.extension.pushBundle
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.base.utils.extension.setGridManager
import com.example.musicapp.base.utils.extension.setLinearLayoutManager
import com.example.musicapp.base.viewmodel.ExploreViewModel
import com.example.musicapp.base.viewmodel.HomeViewModel
import com.example.musicapp.databinding.FragmentExploreBinding
import com.example.musicapp.entities.Artist
import com.example.musicapp.entities.Category
import com.example.musicapp.entities.Song
import com.example.musicapp.screen.home.ArtistIsSongFragment
import com.example.musicapp.screen.home.SongOfCategoryFragment
import com.example.musicapp.screen.play.PlayingFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.Tab


class ExploreFragment : BaseFragment<FragmentExploreBinding>() {
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[ExploreViewModel::class.java]
    }
    private val homeViewModel by lazy {
        ViewModelProvider(requireActivity())[HomeViewModel::class.java]
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }
    private val songInLibraryAdapter by lazy {
        SongInLibraryAdapter()
    }
    private val artistAdapter by lazy {
        AllArtistAdapter()
    }

    private val categoryInLibraryAdapter by lazy {
        CategoryInLibraryAdapter()
    }
    private var data: SearchResponse? = null
    private var songList: List<Song>? = null
    private var categoryList: List<Category>? = null
    private var artistList: List<Artist>? = null

    override fun initListener() {
        binding.titleSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch()
                return@setOnEditorActionListener true
            }
            false
        }
        binding.searching.setOnClickListener {
            performSearch()
        }
        binding.closeInput.setOnClickListener {
            binding.titleSearch.text.clear()
            binding.tabLayout.visibility = View.GONE
            binding.ry.visibility = View.GONE
        }
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: Tab?) {
                if (data != null)
                    displayDataForTab(tab?.text.toString(), data!!)
            }

            override fun onTabUnselected(tab: Tab?) {

            }

            override fun onTabReselected(tab: Tab?) {

            }
        })
        songInLibraryAdapter.setOnClickPlay { item, position ->
            SharePrefUtils.saveKey(Constant.POSITION, position)
            val bundle = pushBundle(Constant.ITEM, item)
            (activity as MainActivity).apply {
                replaceFragment(R.id.main, PlayingFragment(), true, bundle)
                visibility(false)
            }
        }
        songInLibraryAdapter.setOnClickItem { item, position ->
            SharePrefUtils.saveKey(Constant.POSITION, position)
            val bundle = pushBundle(Constant.ITEM, item)
            (activity as MainActivity).apply {
                visibility(false)
                replaceFragment(R.id.main, PlayingFragment(), true, bundle)
            }
        }
        artistAdapter.setOnClickItem { item, _ ->
            (activity as MainActivity).showAndHideDialog(true)
            SharePrefUtils.saveKey(Constant.SINGERID, item?.id)
            SharePrefUtils.saveKey(Constant.NAMEARTIST, item?.fullname)
            val bundle = pushBundle(Constant.ARTIST, item)
            (activity as MainActivity).apply {
                visibility(false)
                replaceFragment(
                    R.id.main,
                    ArtistIsSongFragment(),
                    true,
                    bundle
                )
            }
        }
        categoryInLibraryAdapter.setOnClickItem { item, _ ->
            SharePrefUtils.saveKey(Constant.CATEGORYID, item?.id)
            homeViewModel.getDataSongWithCategory(
                requireContext(),
                accessToken,
                item?.id!!,
                onDone = { data ->
                    Constant.listSongOfCategory = data
                    (requireActivity() as MainActivity).replaceFragment(
                        R.id.main,
                        SongOfCategoryFragment(),
                        true,
                        null
                    )
                    (requireActivity() as MainActivity).visibility(false)
                }, onError = {
                    requireContext().toast(it)
                })
        }
    }

    private fun performSearch() {
        hideKeyboard(binding.titleSearch)
        binding.tabLayout.getTabAt(0)?.select()
        val titleSearch = binding.titleSearch.text.toString()
        if (titleSearch == "") {
            requireContext().toast(getString(R.string.please_enter_content_to_search))
        } else {
            (activity as MainActivity).showAndHideDialog(true)
            viewModel.getDataSearching(accessToken, titleSearch, onDone = {
                (activity as MainActivity).showAndHideDialog(false)
                binding.tabLayout.visibility = View.VISIBLE
                binding.ry.visibility = View.VISIBLE
                displaySongs(it)
                data = it
            }, onError = {
                requireContext().toastLong(it)
            })
        }
    }

    private fun hideKeyboard(editText: EditText) {
        val imm =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)
    }

    private fun displayDataForTab(tab: String, data: SearchResponse) {
        when (tab) {
            getString(R.string.songs) -> displaySongs(data)
            getString(R.string.category) -> displayCategories(data)
            getString(R.string.artists) -> displayArtists(data)
        }
    }

    private fun displaySongs(data: SearchResponse) {
        binding.ry.setLinearLayoutManager(requireContext(),songInLibraryAdapter,RecyclerView.VERTICAL)
        if (data.data.musics.isEmpty()) {
            binding.nothingData.visibility = View.VISIBLE
            binding.ry.visibility = View.GONE
        } else {
            binding.nothingData.visibility = View.GONE
            binding.ry.visibility = View.VISIBLE
            val musicList: List<Music> = data.data.musics
            songList = musicList.map { convertToSong(it) }
            songInLibraryAdapter.setDataList(songList!!)
        }
    }

    private fun displayCategories(data: SearchResponse) {
        binding.ry.setLinearLayoutManager(requireContext(),categoryInLibraryAdapter,RecyclerView.VERTICAL)
        if (data.data.categories.isEmpty()) {
            binding.nothingData.visibility = View.VISIBLE
            binding.ry.visibility = View.GONE
        } else {
            binding.nothingData.visibility = View.GONE
            binding.ry.visibility = View.VISIBLE
            categoryList = data.data.categories.map { convertToCategory(it) }
            categoryInLibraryAdapter.setDataList(categoryList!!)
        }
    }

    private fun displayArtists(data: SearchResponse) {
        binding.ry.setGridManager(requireContext(),2,artistAdapter)
        if (data.data.singers.isEmpty()) {
            binding.nothingData.visibility = View.VISIBLE
            binding.ry.visibility = View.GONE
        } else {
            binding.nothingData.visibility = View.GONE
            binding.ry.visibility = View.VISIBLE
            artistList = data.data.singers.map { convertToArtist(it) }
            artistAdapter.setDataList(artistList!!)
        }
    }

    private fun convertToArtist(it: Singer): Artist {
        return Artist(
            fullname = it.fullname,
            birthday = it.birthday,
            gender = it.gender,
            avatar = it.avatar,
            description = it.description,
            id = it.id
        )
    }

    private fun convertToCategory(it: Category1): Category {
        return Category(
            name = it.name,
            image = it.image,
            id = it.id,
            createdAt = it.createdAt
        )
    }

    private fun convertToSong(music: Music): Song {
        return Song(
            id = music.id,
            title = music.title,
            singer = music.singer.id,
            description = music.description,
            country = music.country,
            image = music.image,
            audio = music.audio,
            year = music.year,
            length = music.length,
            playCount = music.playCount,
            category = music.categoryId
        )
    }

    override fun initData() {

    }

    override fun initView() {
        initItemTabLayout(getString(R.string.songs))
        initItemTabLayout(getString(R.string.category))
        initItemTabLayout(getString(R.string.artists))
    }

    override fun inflateLayout(
        inflater: LayoutInflater,

        container: ViewGroup?
    ): FragmentExploreBinding {
        return FragmentExploreBinding.inflate(inflater, container, false)
    }

    private fun initItemTabLayout(title: String) {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(title))
    }
}