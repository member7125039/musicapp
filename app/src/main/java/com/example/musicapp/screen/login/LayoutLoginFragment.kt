package com.example.musicapp.screen.login

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.musicapp.R
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.databinding.FragmentLayoutLoginBinding
import com.example.musicapp.screen.sign_up.SignUpFragment


class LayoutLoginFragment : BaseFragment<FragmentLayoutLoginBinding>() {
    private lateinit var activity: LoginActivity
    override fun initListener() {
        binding.goLayoutSignIn.setOnClickListener {
            activity.replaceFragment(R.id.login, SignInFragment(), true, null)
        }
        binding.tvSignUp.setOnClickListener {
            activity.replaceFragment(R.id.login, SignUpFragment(), true, null)
        }
    }

    override fun initData() {

    }

    override fun initView() {
        activity = requireActivity() as LoginActivity
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentLayoutLoginBinding {
        return FragmentLayoutLoginBinding.inflate(inflater, container, false)
    }
}