package com.example.musicapp.screen.home


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.adapter.SongInArtistAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.ExtensionFunction.toast
import com.example.musicapp.base.utils.extension.getDataSerializable
import com.example.musicapp.base.utils.extension.setLinearLayoutManager
import com.example.musicapp.base.utils.formatBirthday
import com.example.musicapp.base.viewmodel.HomeViewModel
import com.example.musicapp.databinding.FragmentArtistIsSongBinding
import com.example.musicapp.entities.Artist

class ArtistIsSongFragment : BaseFragment<FragmentArtistIsSongBinding>() {
    private val viewModel by lazy {
        ViewModelProvider(requireActivity())[HomeViewModel::class.java]
    }
    private val songInArtistAdapter by lazy {
        SongInArtistAdapter(nameArtist)
    }

    private val nameArtist by lazy {
        SharePrefUtils.getString(Constant.NAMEARTIST)
    }
    private val accessToken by lazy {
        SharePrefUtils.getString(Constant.ACCESS_TOKEN)
    }

    override fun initListener() {
        binding.backArtistInSong.setOnClickListener {
            (requireActivity() as MainActivity).backFragment()
        }
        songInArtistAdapter.setOnClickItem { item, _ ->
            (activity as MainActivity).showAndHideDialog(true)
            FunctionUtils.playingSongChoose((activity as MainActivity), item)
        }
        songInArtistAdapter.setOnClickPlay { item, _ ->
            (activity as MainActivity).showAndHideDialog(true)
            FunctionUtils.playingSongChoose((activity as MainActivity), item)
        }
    }

    override fun initData() {

    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        arguments?.getDataSerializable(Constant.ARTIST, Artist::class.java)?.let { artist ->
            initViewArtist(artist)
        }
        binding.listSong.setLinearLayoutManager(requireContext(), songInArtistAdapter,RecyclerView.VERTICAL)
        getSongOfArtist()
    }

    private fun getSongOfArtist() {
        val singerId = SharePrefUtils.getString(Constant.SINGERID)
        viewModel.getSongOfArtist(accessToken, singerId, onDone = {
            songInArtistAdapter.setDataList(it)
            (activity as MainActivity).showAndHideDialog(false)
        }, onError = {
            requireContext().toast(it)
        })
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentArtistIsSongBinding {
        return FragmentArtistIsSongBinding.inflate(inflater, container, false)
    }

    private fun initViewArtist(item: Artist?) {
        FunctionUtils.loadImage(requireContext(), binding.avatarArtist, item?.avatar, 30)
        binding.nameArtist.text = item?.fullname
        val birthday = item?.birthday
        val startIndex = birthday?.indexOf('"')?.plus(1)
        val endIndex = birthday?.indexOf('T')

        if (startIndex != null && endIndex != null && startIndex < endIndex) {
            val date = birthday.substring(startIndex, endIndex)
            binding.birthday.text = date.formatBirthday()
        }
    }

}