package com.example.musicapp.screen.library


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.R
import com.example.musicapp.adapter.SongInLibraryAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.pushBundle
import com.example.musicapp.base.utils.extension.replaceFragment
import com.example.musicapp.base.utils.extension.setLinearLayoutManager
import com.example.musicapp.databinding.FragmentAllSongInLibraryBinding
import com.example.musicapp.screen.play.PlayingFragment

class AllSongInLibraryFragment : BaseFragment<FragmentAllSongInLibraryBinding>() {
    private val songInLibraryAdapter by lazy {
        SongInLibraryAdapter()
    }

    override fun initListener() {
        binding.backAllSongInLibrary.setOnClickListener {
            (activity as MainActivity).backFragment()
        }
        songInLibraryAdapter.setOnClickPlay { item, position ->
            SharePrefUtils.saveKey(Constant.POSITION, position)
            val bundle = pushBundle(Constant.ITEM, item)
            (activity as MainActivity).replaceFragment(R.id.main, PlayingFragment(), true, bundle)
        }
        songInLibraryAdapter.setOnClickItem { item, position ->
            SharePrefUtils.saveKey(Constant.POSITION, position)
            val bundle = pushBundle(Constant.ITEM, item)
            (activity as MainActivity).replaceFragment(R.id.main, PlayingFragment(), true, bundle)
        }
    }

    override fun initData() {
        songInLibraryAdapter.setDataList(Constant.listSong)
    }

    override fun initView() {
        binding.listSong.setLinearLayoutManager(requireContext(), songInLibraryAdapter,RecyclerView.VERTICAL)
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAllSongInLibraryBinding {
        return FragmentAllSongInLibraryBinding.inflate(inflater, container, false)
    }
}