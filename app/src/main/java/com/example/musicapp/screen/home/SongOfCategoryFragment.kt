package com.example.musicapp.screen.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.MainActivity
import com.example.musicapp.adapter.SongInCategoryAdapter
import com.example.musicapp.base.base_view.BaseFragment
import com.example.musicapp.base.utils.Constant
import com.example.musicapp.base.utils.FunctionUtils
import com.example.musicapp.base.utils.SharePrefUtils
import com.example.musicapp.base.utils.extension.setLinearLayoutManager
import com.example.musicapp.base.utils.extension.setLinearLayoutManagerVertical
import com.example.musicapp.databinding.FragmentSongOfCategoryBinding


class SongOfCategoryFragment : BaseFragment<FragmentSongOfCategoryBinding>() {
    private val songInCategoryAdapter by lazy {
        SongInCategoryAdapter(idCategory)
    }
    private val idCategory by lazy {
        SharePrefUtils.getString(Constant.CATEGORYID)
    }

    override fun initListener() {
        binding.backSOngOfCategory.setOnClickListener {
            (requireActivity() as MainActivity).backFragment()
        }
        songInCategoryAdapter.setOnClickItem { item, _ ->
            (activity as MainActivity).showAndHideDialog(true)
            FunctionUtils.playingSongChoose((requireActivity() as MainActivity), item)
        }
        songInCategoryAdapter.setOnClickPlay { item, _ ->
            (activity as MainActivity).showAndHideDialog(true)
            FunctionUtils.playingSongChoose((requireActivity() as MainActivity), item)
        }
    }

    override fun initData() {

    }

    override fun initView() {
        SharePrefUtils.init(requireContext())
        songInCategoryAdapter.setDataList(Constant.listSongOfCategory)
        binding.listSong.setLinearLayoutManager(requireContext(), songInCategoryAdapter,RecyclerView.VERTICAL)
        val idCategory = SharePrefUtils.getString(Constant.NAMECATEGORY)
        Constant.listCategory.find { it.id == idCategory }?.let {
            binding.nameCategory.text = it.name
        }
        (activity as MainActivity).showAndHideDialog(false)
    }

    override fun inflateLayout(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSongOfCategoryBinding {
        return FragmentSongOfCategoryBinding.inflate(inflater, container, false)
    }
}